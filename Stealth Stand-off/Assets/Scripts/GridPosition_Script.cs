﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPosition_Script : MonoBehaviour
{
    [SerializeField] private bool isPlayerHere;
    public GameObject player;
    public GameObject[] adjacentGridPos;
}