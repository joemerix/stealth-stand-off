﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public Transform currentPos;
    public bool live;
    [SerializeField] private float speed = 5f;

    public void Movement (Transform target)
    {
        if (!live)
            return;
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
}
